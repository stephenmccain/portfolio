import Link from "next/link"
import React from "react"
import ThemeSwitch from "./ThemeSwitch.js"

 


const Navigation = () => {
    return (
        <div className="sticky top-0 z-20 py-2 bg-white md:py-6 md:mb-6 dark:bg-black">
            <div className="container px-4 mx-auto lg:max-w-4xl flex items-center justify-between">
                <Link href="/">
                    <a
                        className={"font-medium tracking-wider transition-colors text-gray-900 hover:text-sky-500 uppercase dark:text-white"}
                    >
                        Stephen "Matty" McCain  
                        
                    </a>
                </Link>
                <div className="md:flex lg:flex space-x-4 font-medium text-gray-800 hidden sm:black dark:text-white">
                    <a className="transition-colors hover:text-sky-500" target="true" rel="noreferrer" href="/">
                        <div>About</div>
                    </a>
                    <Link href="/projects">
                    <a className="transition-colors hover:text-sky-500" target="true" rel="noreferrer" >
                        <div>Projects</div>
                    </a>
                    </Link>
                    <Link href="/resume">
                    <a className="transition-colors hover:text-sky-500" target="true" rel="noreferrer" >
                        <div>Resume</div>
                    </a>
                    </Link>
                    <Link href="/contractwork">
                    <a className="transition-colors hover:text-sky-500" target="true" rel="noreferrer" >
                        <div>Contract Work</div>
                    </a>
                    </Link>

                </div>
                <ThemeSwitch />
            </div>
        </div>
    )
}

export default Navigation;