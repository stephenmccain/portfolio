import React from "react"
import Image from "next/image"
import profile1 from "../public/profile1.jpeg"

 



const About = () => {
    return (
        
        <>
        <div className="container px-4 mx-auto">
            <div className="lg:space-x-5 lg:flex lg:flex-row item-center lg:-mx-4 flex flex-col-reverse text-center lg:text-left">
                <div className="lg:px-4 lg:mt-12 ">
                    <h1 className="text-2xl font-bold text-gray-900 lg:text-5xl dark:text-white">
                        Software Engineer
                    </h1>
                    <div className="mt-6 text-gray-800 dark:text-white">
                        <p className="mb-4"> 
                          Stephen "Matty" McCain here, I'm a Software Engineer remotely based in Bozeman, Montana. 
                            I enjoy working with others to create, build and deploy cleanly written and UI/UX driven software.
                            My values revolve around <strong>diversity, clarity, empathy and integrity</strong>.
                        </p>
                    </div>
                    <div className="mt-6 text-gray-800 dark:text-white">
                        <p className="mb-4"> 
                        As Vanilla Ice said..."And if there was a problem Yo, I'll solve it Check out the hook while my DJ revolves it".
                        </p>
                        </div>
                        <div className="mt-6 text-gray-800 dark:text-white">
                        <p className="mb-4"> 
                            I have a diverse background ranging from being a zamboni driver, archaeologist in Israel, professional photographer, 
                            senior commercial auto/injury claims investigator to being a multi-day river guide. My life has taking all over
                            the world and in those travels I have always been able to adapt and learn local culture and ways.
                            
                        </p>
                        </div>
                        <div className="mt-6 text-gray-800 dark:text-white">
                        <p className="mb-4">
                             What was a goal I have completed in my life?</p>
                        <p> - In 2005, I completed a 6 month thru-Hike of the Appalachian Trail</p>
                    </div>
                </div>
                <div className="flex-shrink-0 lg:mt-12 lg:px-4 mb-15">
                    <Image
                        src={profile1}
                        alt="Profile"
                        priority={true}
                        className="rounded-full"
                        width={300}
                        height={350}
                        placeholder="blur"
                    />

                    <p style={{textAlign: "center"}} >Full Stack Software Engineer</p>
                    <p style={{textAlign: "center"}} >Bozeman, MT 406-579-6972</p>
                    <p style={{textAlign: "center"}} >mattymccain@gmail.com</p>
                </div>
            </div>
            <div className="mt-6 text-gray-800 dark:text-white">
            <h1 className="text-2xl font-bold text-gray-900 lg:text-5xl dark:text-white">
                        Skills:
                        
            
                    </h1>
                    <bk></bk>
            </div>
                    
        </div>

        
 
        </>
    )
}

export default About;