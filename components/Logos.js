import React from "react"
import Image from "next/image"
import python from "../logos/python.svg"
import js_logo from "../logos/js_logo.svg"
import react_logo from "../logos/react_logo.svg"
import docker_logo from "../logos/docker_logo.svg"
import sql_logo from "../logos/sql_logo.svg"
import django_logo from "../logos/django_logo.svg"
import mongodb_logo from "../logos/mongodb_logo.svg"
import html_logo from "../logos/html_logo.svg"
import css_logo from "../logos/css_logo.svg"
import rubyOnRails_logo from "../logos/rubyOnRails_logo.svg"

export default function Logos() {
  return (
    <>
        <div className=" flex items-center justify-center md:flex lg:flex space-x-4 font-medium text-gray-800 hidden sm:black dark:text-white ">
        <section>
        <Image
                        src={html_logo}
                        alt="html_logo"
                        priority={true}
                        className="rounded-full"
                        width={50}
                        height={50} 
                    />
                    <p>HTML5</p>
                    </section>
        <section>
        <Image
                        src={css_logo}
                        alt="css_logo"
                        priority={true}
                        className="rounded-full"
                        width={50}
                        height={50} 
                    />
                    <p>CSS3</p>
                    </section>
        <section>
        <Image
                        src={python}
                        alt="python"
                        priority={true}
                        className="rounded-full"
                        width={50}
                        height={50} 
                    />
                    <p>Python3</p>
                    </section>
                    <section>
                    <Image
                        src={django_logo}
                        alt="django_logo"
                        priority={true}
                        className="rounded-full"
                        width={50}
                        height={50}
                        stroke="white"
                        stroke-width={20}
                    />
                    <p>Django</p>
                    </section>
                    <section>
                    <Image
                        src={js_logo}
                        alt="js_logo"
                        priority={true}
                        className="rounded-full"
                        width={50}
                        height={50}
                        
                    />
                    <p textAlign="left" >JavaScript</p>
                    </section>
                    <section>
                    <Image
                        src={react_logo}
                        alt="react_logo"
                        priority={true}
                        className="rounded-full"
                        width={50}
                        height={50}
                        
                    />
                    <p>React</p>
                    </section>
                    <section>
                    <Image
                        src={docker_logo}
                        alt="docker_logo"
                        priority={true}
                        className="rounded-full"
                        width={50}
                        height={50}
                        
                    />
                    <p>Docker</p>
                    </section>
                    <section>
                    <Image
                        src={sql_logo}
                        alt="sql_logo"
                        priority={true}
                        className="rounded-full"
                        width={50}
                        height={50}
                    />
                    <p>postgreSQL</p>
                    </section>
                    <section>
                    <Image
                        src={mongodb_logo}
                        alt="mongodb_logo"
                        priority={true}
                        className="rounded-full"
                        width={50}
                        height={50}
                    />
                    <p>MongoDB</p>
                    </section>
                    <section>
                    <Image
                        src={rubyOnRails_logo}
                        alt="rubyOnRails_logo"
                        priority={true}
                        className="rounded-full"
                        width={100}
                        height={50}
                    />
                    <p>Ruby on Rails</p>
                    </section>
                    

                    
                    
        </div>
        </>

  );
}