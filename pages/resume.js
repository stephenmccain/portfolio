
import Image from "next/image"
import React from "react";
import resume from "../public/resume.jpeg"
import resume1 from "../public/resume1.jpeg"
import resume2 from "../public/resume2.jpeg"


export default function App() {
  return (
    <>
    <div className="flex-shrink-0 lg:mt-12 lg:px-4 mb-10" style={{ textAlign: "center" }}>
                    <Image
                        src={resume1}
                        alt="resume1"
                        priority={true}
                        layout="intrinsic"
                        placeholder="blur"
                    />
                    </div>

    <div className="flex-shrink-0 lg:mt-12 lg:px-4 mb-10" style={{ textAlign: "center" }}>
                    <Image
                        src={resume2}
                        alt="resume2"
                        priority={true}
                        layout="intrinsic"
                        placeholder="blur"
                    />
    </div>
    </>
  );
}

