
import Head from 'next/head'
import About from "../components/About"
import Logos from "../components/Logos";

export default function Home() {
  return (
    <div className="space-y-14 lg:space-y-24">
      <Head>
        <title>Stephen McCain</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className="max-w-4xl mx-auto mt-16 antialiased">
        
        <About/>
        <bk></bk>
        <div className="p-9 ">

        <Logos/>
        </div>
          
            
         
         
        
        
        
        
        
        
      </main>
    </div>
  )
}
