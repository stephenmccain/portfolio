import Image from "next/image"

import homesideApp from "../public/homesideApp.jpeg"
import homesidecode from "../public/homesidecode.jpeg"




export default function Contractwork() {
    return (
        <>
            <div className="mt-12 lg:mt-18 sm:pb-36 sm:py-12 py-6">
                <div className="max-w-4xl px-4 mx-auto text-gray-800 dark:text-white">
                    {/* <div className="pb-8 mb-2 border-t-2 border-gray-300 dark:border-white-300"></div> */}
                    <h1 style={{ textAlign: "center" }}> Contract Work</h1>
                    <br></br>
                    <h1> HomeSide Financial / Lower:</h1>
                    <br></br>
                    <div className="flex flex-col justify-between lg:flex-row items-center">
                        <h1>  
                            <a
                                
                                href="https://gitlab.com/stephenmccain/homeside"
                                className={"transition-colors hover:text-yellow-500 "}
                                target="_blank"
                                rel="noreferrer"
                            > 
                                 https://gitlab.com/stephenmccain/homeside
                            </a>

                            <p>
                                - Contract employment with HomeSide Financial for assistance with networking Webflow and Airtable. The marketing department
                                 was having issues with connecting Webflow to their loan officer Airtable database. API call were being made via Zapier, the 
                                 goal of the project was to remove Zapier from the workflow as the cost of using the 3rd party application was costing the company unneeded money.
                                 After review of Webflow and Airtable documents, it was noted that the user was unable to make numerous API calls per day. This is why the company was using Zapier.
                                I built a React application to make the API call to Airtable. To make the repeated API calls to the intervial the marketing department needed per the requirements, 
                                the React application was updated to refresh the API every 15 minutes to fulfill their needs

                            </p>


                        </h1>

                    </div>
                </div>
                <div className="flex-shrink-0 lg:mt-12 lg:px-4 mb-10" style={{ textAlign: "center" }}>

                    <Image
                        src={homesideApp}
                        alt="homesideApp"
                        priority={true}
                        // className="rounded-full"
                        width={300}
                        height={200}
                        placeholder="blur"
                        
                    />
                    {' '}
                    <Image
                        src={homesidecode}
                        alt="homesidecode"
                        priority={true}
                        // className="rounded-full"
                        width={300}
                        height={200}
                        placeholder="blur"
                    />
                    
                </div>
                
                
                

                </div>
        </>

    );
}