import 'tailwindcss/tailwind.css'
import Navigation from "../components/Navigation";
import Footer from "../components/Footer";
import { ThemeProvider } from 'next-themes'
import ReactGA from 'react-ga';

const TRACKING_ID = "G-53TYP9HYFF"; // YOUR_OWN_TRACKING_ID
ReactGA.initialize(TRACKING_ID);


function MyApp({Component, pageProps}) {
    return <>
            <ThemeProvider attribute="class" enableSystem={true}>
            <Navigation/>
            <Component {...pageProps} />

            <Footer/>
            </ThemeProvider>
    </>
}

export default MyApp