import Image from "next/image"
import React from "react"
import isella1 from "../public/isella1.jpeg"
import isella2 from "../public/isella2.jpeg"
import isella3 from "../public/isella3.jpeg"
import rivertrip1 from "../public/rivertrip1.jpeg"
import rivertrip2 from "../public/rivertrip2.jpeg"
import rivertrip3 from "../public/rivertrip3.jpeg"
import bigsteve from "../public/bigsteve.jpeg"
import bigsteve1 from "../public/bigsteve1.jpeg"
import bigsteve2 from "../public/bigsteve2.jpeg"
import ModalImage from "react-modal-image"
import Image0 from "../pages/image0.jpeg"

export default function Projects() {
    
    return (
        <>
            <div className="mt-12 lg:mt-18 sm:pb-36 sm:py-12 py-6">
                <div className="max-w-4xl px-4 mx-auto text-gray-800 dark:text-white">
                    {/* <div className="pb-8 mb-2 border-t-2 border-gray-300 dark:border-white-300"></div> */}
                    <h1 style={{ textAlign: "center" }}> Deployed Projects</h1>
                    <br></br>
                    <h1> ISELLA:</h1>
                    <br></br>
                    <div className="flex flex-col justify-between lg:flex-row items-center">
                        <h1>  
                            <a
                                
                                href="https://binary-buddies.gitlab.io/project-delta/"
                                className={"transition-colors hover:text-yellow-500 "}
                                target="_blank"
                                rel="noreferrer"
                            > 
                                 https://binary-buddies.gitlab.io/project-delta/
                            </a>

                            <p>
                                - An e-commerce site focused on selling used outdoor clothing and technical gear. User ability to login, upload gear for sale
                                and communicate with possible buyers via email. Python and Django based application with a React frontend. Multiple microservices, API calls
                                inside out outside of application. Utilized Google and OpenWeather API call as well as image hosting on AWE S3.
                            </p>


                        </h1>

                    </div>
                </div>

                <div className="flex-shrink-0 lg:mt-12 lg:px-4 mb-10" style={{ textAlign: "center" }}>
                {/* <ModalImage
                        className="thumb"
                        small={Image0.jpeg}
                        large={Image0}
                        alt="test"
                        /> */}
                    <Image
                        src={isella1}
                        alt="isella1"
                        priority={true}
                        // className="rounded-full"
                        width={300}
                        height={200}
                        placeholder="blur"
                    />
                    {' '}
                    <Image
                        src={isella2}
                        alt="isella2"
                        priority={true}
                        // className="rounded-full"
                        width={300}
                        height={200}
                        placeholder="blur"
                    />
                    {' '}
                    <Image
                        src={isella3}
                        alt="isella3"
                        priority={true}
                        // className="rounded-full"
                        width={300}
                        height={200}
                        placeholder="blur"
                    />
                </div>
                <div className="max-w-4xl px-4 mx-auto text-gray-800 dark:text-white">
                    {/* <div className="pb-8 mb-2 border-t-2 border-gray-300 dark:border-white-300"></div> */}
                    <h1> RIVER TRIP:</h1>
                    <br></br>
                    <div className="flex flex-col justify-between lg:flex-row items-center">
                        <h1> 
                            <a
                                href="https://rivertrips.herokuapp.com/accounts/login/"
                                className={"transition-colors hover:text-yellow-500"}
                                target="_blank"
                                rel="noreferrer"
                            >
                                https://rivertrips.herokuapp.com/accounts/login/
                            </a>

                            <p>
                                - An activity tracking application for river floats and trips, for personal or professional use.
                                Python and Django based application with an HTML frontend. Monolith design with user authentication via Django 
                                LoginRequiredMixin. Users login needed to create, edit, delete trips. Resources links provided along with food and gear list.
                            </p>


                        </h1>

                    </div>
                </div>
                <div className="flex-shrink-0 lg:mt-12 lg:px-4 mb-10" style={{ textAlign: "center" }}>
                    <Image
                        src={rivertrip1}
                        alt="rivertrip1"
                        priority={true}
                        // className="rounded-full"
                        width={300}
                        height={200}
                        placeholder="blur"
                    />
                    {' '}
                    <Image
                        src={rivertrip2}
                        alt="rivertrip2"
                        priority={true}
                        // className="rounded-full"
                        width={300}
                        height={200}
                        placeholder="blur"
                    />
                    {' '}
                    <Image
                        src={rivertrip3}
                        alt="rivertrip3"
                        priority={true}
                        // className="rounded-full"
                        width={300}
                        height={200}
                        placeholder="blur"
                    />
                </div>
                {/* line break added below */}
                <div className="mt-12 lg:mt-18 sm:pb-36 sm:py-12 py-6">
                    <div className="max-w-4xl px-4 mx-auto text-gray-800 dark:text-white">
                    <div className="pb-8 mb-2 border-t-2 border-gray-300 dark:border-white-300"></div>
                    <div className="flex flex-col justify-between lg:flex-row items-center">

                </div>
                </div>
                </div>

                <div className="max-w-4xl px-4 mx-auto text-gray-800 dark:text-white">
                    {/* <div className="pb-8 mb-2 border-t-2 border-gray-300 dark:border-white-300"></div> */}
                    <h1 style={{ textAlign: "center", fontSize: "20px "}}> Other Projects</h1>
                    <br></br>
                    <h1> BIG STEVE'S AUTO EMPORIUM:</h1>
                    <br></br>
                    <div className="flex flex-col justify-between lg:flex-row items-center">
                        <h1> 
                            <a
                                href="https://gitlab.com/stephenmccain/project-beta"
                                className={"transition-colors hover:text-yellow-500"}
                                target="_blank"
                                rel="noreferrer"
                            >
                                https://gitlab.com/stephenmccain/project-beta
                            </a>

                            <p>
                                - Auto sales application using Python, Django and React. User ability to add vehicle, employee, customer and sales details. 
                                Poller employed to obtain inventory data from database (Docker). Backend utilized ForeignKeys to relate models for additional
                                functionality, as well as use of Encoders. This was a team project of 2 engineers (my responsibility was the Sales, Customer and
                                Employee models) with the frontend being powered by React. Additional features were added once course requirements we met, including 
                                React Confetti.

                            </p>


                        </h1>

                    </div>
                </div>
                <div className="flex-shrink-0 lg:mt-12 lg:px-4 mb-10" style={{ textAlign: "center" }}>
                    <Image
                        src={bigsteve}
                        alt="bigsteve"
                        priority={true}
                        // className="rounded-full"
                        width={300}
                        height={200}
                        placeholder="blur"
                    />
                    {' '}
                    <Image
                        src={bigsteve2}
                        alt="bigsteve2"
                        priority={true}
                        // className="rounded-full"
                        width={300}
                        height={200}
                        placeholder="blur"
                    />
                    {' '}
                    <Image
                        src={bigsteve1}
                        alt="bigsteve1"
                        priority={true}
                        // className="rounded-full"
                        width={300}
                        height={200}
                        placeholder="blur"
                    />
                </div>
                
            </div>
        </>

    );
}